package com.example.myfindme

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createInstructionsDialog().show()
    }

    private fun createInstructionsDialog(): Dialog {
        return AlertDialog.Builder(this).apply {
            setIcon(R.drawable.android)
            setTitle(R.string.instructions_title)
            setMessage(R.string.instructions)
            setPositiveButtonIcon(
                ContextCompat.getDrawable(
                    context, android.R.drawable.ic_media_play
                )
            )
        }.create()
    }

}