What's this?
=============
Ａ simple practice following [Advanced Android in Kotlin 02.4:Creating Effects with Shaders] in CodeLabs.
<br>It looks like a gaming app, I am really excited！

Friendly reminder
=============
During Task: Creating the BitmapShader, I am a little confused so I want to make a small summary,
<br>in the init scope the final production is a Paint with a BitmapShader and the BitmapShader with 
<br>a Bitmap and the Bitmap drawn on a rectangle and a spotlight.

<br> Here are independent knowledge which also help you understand this project more:
- [PorterDuff.Mode]
- [TileMode]
- [BitmapShader]

[BitmapShader]:https://developer.android.com/reference/android/graphics/BitmapShader.html
[TileMode]:https://developer.android.com/reference/android/graphics/Shader.TileMode.html
[PorterDuff.Mode]:https://developer.android.com/reference/android/graphics/PorterDuff.Mode
[Advanced Android in Kotlin 02.4:Creating Effects with Shaders]:https://developer.android.com/codelabs/advanced-android-kotlin-training-shaders#0
